<?php

namespace AppBundle\Controller;

use M4U\DashboardBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login_form")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $session = $request->getSession();

        if (class_exists('\Symfony\Component\Security\Core\Security'))
        {
            $authErrorKey = Security::AUTHENTICATION_ERROR;
            $lastUsernameKey = Security::LAST_USERNAME;
        }
        else
        {
            // BC for SF < 2.6
            $authErrorKey = SecurityContextInterface::AUTHENTICATION_ERROR;
            $lastUsernameKey = SecurityContextInterface::LAST_USERNAME;
        }

        if ($request->attributes->has($authErrorKey))
        {
            $error = $request->attributes->get($authErrorKey);
        }
        elseif (null !== $session && $session->has($authErrorKey))
        {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        }
        else
        {
            $error = null;
        }

        if (!$error instanceof AuthenticationException)
        {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        if ($this->has('security.csrf.token_manager'))
        {
            $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        }
        else
        {
            // BC for SF < 2.4
            $csrfToken = $this->has('form.csrf_provider')
                ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
                : null;
        }

        $registration = new User();
        $form = $this->createForm('AppBundle\Form\Type\RegistrationType', $registration, ['action' => $this->generateUrl('register'), 'method' => 'POST']);

        return $this->render(
            'security/login.html.twig',
            [
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
                'form' => $form->createView(),
                'csrf_token' => $csrfToken,
            ]
        );
    }
}