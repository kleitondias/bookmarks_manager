<?php

namespace AppBundle\Controller;

use Acme\UserBundle\Entity\UserRepository;
use M4U\DashboardBundle\Entity\User;
use Acme\UserBundle\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle;

class BaseController extends Controller
{

    public function getUser()
    {
        $session = new Session();
        $userId = $session->get('userId');
        $product = $this->getDoctrine()->getRepository('M4UDashboardBundle:User')->find($userId);
        return $product;
    }

}