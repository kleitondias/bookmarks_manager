<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use M4U\DashboardBundle\Entity\User;

/**
 * @Route("/api/users")
 */
class UsersApiController extends BaseApiController
{
    /**
     * @Route("", name="api_users")
     * @Method({"GET"})
     */
    public function listAction()
    {
        return parent::listAction();
    }

    /**
     * @Route("/{id}", name="api_users_read")
     * @Method({"GET"})
     */
    public function readAction($id)
    {
        return parent::readAction($id);
    }

    /**
     * @Route("", name="api_users_create")
     * @Method({"POST"})
     */
    public function createAction()
    {
        return parent::createAction();
    }

    /**
     * @see BaseApiController::getRepository()
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository('M4UDashboardBundle:User');
    }

    /**
     * @see BaseApiController::getNewEntity()
     * @return Object
     */
    public function getNewEntity()
    {
        return new User();
    }
}