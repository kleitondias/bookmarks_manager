<?php

namespace AppBundle\Controller;

use M4U\DashboardBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    public function registerAction(Request $req)
    {
        $em   = $this->getDoctrine()->getManager();
        $form = $this->createForm('AppBundle\Form\Type\RegistrationType', new User());
        $form->handleRequest($req);

        if ($form->isValid())
        {
            $user = new User();
            $user = $form->getData();

            $user->setCreatedAt(new \DateTime());
            $user->setEnabled(true);
            $user->addRole(User::USER);

            $pwd = $user->getPassword();
            $encoder = $this->container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($user, $pwd);
            $user->setPassword($pwd);

            $em->persist($user);
            $em->flush();

            $url = $this->generateUrl('login');
            return $this->redirect($url);
        }
        else
        {

            return $this->render(
                'security/register.html.twig',
                [
                    'error' => null,
                    'form' => $form->createView()
                ]
            );

        }
    }

}
