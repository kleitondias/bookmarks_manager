<?php

namespace AppBundle\Controller;

use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use M4U\DashboardBundle\Entity\Bookmark;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api/bookmarks")
 */
class BookmarksApiController extends BaseApiController
{
    /**
     * @Route("", name="api_bookmarks")
     * @Method({"GET"})
     */
    public function listAction()
    {
        return parent::listAction();
    }

    /**
     * @Route("/{id}", name="api_bookmarks_by_user_id")
     * @Method({"GET"})
     */
    public function listByUserIdAction($id)
    {
        $list = $this->getRepository()->createQueryBuilder('e')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        return new JsonResponse($list);
    }

    /**
     * @Route("/{id}", name="api_bookmarks_read")
     * @Method({"GET"})
     */
    public function readAction($id)
    {
        return parent::readAction($id);
    }

    /**
     * @Route("", name="api_bookmarks_create")
     * @Method({"POST"})
     */
    public function createAction()
    {
        return parent::createAction();
    }

    /**
     * @see BaseApiController::getRepository()
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository('M4UDashboardBundle:Bookmark');
    }

    /**
     * @see BaseApiController::getNewEntity()
     * @return Object
     */
    public function getNewEntity()
    {
        return new Bookmark();
    }
}