<?php

namespace M4U\DashboardBundle\Controller;

use AppBundle\Controller\BaseController;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Controller\BookmarksApiController;
use FOS\RestBundle\Tests\Functional\WebTestCase;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\View;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use M4U\DashboardBundle\Entity\Bookmark;
use M4U\DashboardBundle\Form\BookmarkType;
use M4U\DashboardBundle\Model\BookmarkInterface;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Bookmark controller.
 *
 * @Route("/bookmark")
 */
class BookmarkController extends BaseController
{
    /**
     * Lists all Bookmark entities.
     *
     * @Route("/", name="bookmark_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        WebTestCase::createClient();
        $parameters = array();
        $bookmarkApi = new Bookmark();


        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_ADMIN'))
        {
            $route =  $this->generateUrl('api_users', array($parameters, '_format' => 'json'));
            $request = $client->request('GET', $route, array('ACCEPT' => 'application/json'));
            $response = $client->getResponse();
            $obj = $response->getContent();
            $response->send();
            $kernel->terminate($request, $response);
            $users = json_decode($obj);
            //$users = $bookmarkApi->createRequestForApi($client, $route, 'GET');
            $bookmarks = null;
        }
        else
        {
            //$parameters['id'] = $user->getId();
            //$route =  $this->generateUrl('api_bookmarks_by_user_id', array('id' => $user->getId(), '_format' => 'json'));
            //$bookmarks = $bookmarkApi->createRequestForApi($client, $route, 'GET');
            //$bookmarks = $em->getRepository('M4UDashboardBundle:Bookmark')->findBy(['userId' => $user->getId()]);
            $users = null;
        }

        print_r($users);
        return $this->render('bookmark/index.html.twig', array(
            'user' => $user,
            'bookmarks' => $bookmarks,
            'users' => $users,
        ));
    }

    /**
     * Creates a new Bookmark entity.
     *
     * @Route("/new", name="bookmark_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $errors = null;
        $user = $this->getUser();
        $userId = $user->getId();
        $bookmark = new Bookmark();
        $form = $this->createForm('M4U\DashboardBundle\Form\BookmarkType', $bookmark);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $bookmark->setUser($user);
            $bookmark->setCreatedAt(new \DateTime());
            $em->persist($bookmark);
            $em->flush();

            return $this->redirectToRoute('bookmark_index');
        }
        else
        {
            $errors = $form->getErrors();

            return $this->render('bookmark/new.html.twig', array(
                'bookmark' => $bookmark,
                'form' => $form->createView(),
                'user' => $user,
                'errors' => $errors,
            ));
        }

        return $this->render('bookmark/new.html.twig', array(
            'bookmark' => $bookmark,
            'form' => $form->createView(),
            'user' => $user,
            'errors' => $errors,
        ));
    }

    /**
     * Finds and displays a Bookmark entity.
     *
     * @Route("/{id}", name="bookmark_show")
     * @Method("GET")
     */
    public function showAction(Bookmark $bookmark)
    {
        $user = $this->getUser();
        $deleteForm = $this->createDeleteForm($bookmark);

        return $this->render('bookmark/show.html.twig', array(
            'bookmark' => $bookmark,
            'delete_form' => $deleteForm->createView(),
            'user' => $user,
        ));
    }

    /**
     * Displays a form to edit an existing Bookmark entity.
     *
     * @Route("/{id}/edit", name="bookmark_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Bookmark $bookmark)
    {
        $errors = null;
        $user = $this->getUser();
        $editForm = $this->createForm('M4U\DashboardBundle\Form\BookmarkType', $bookmark);
        $editForm->handleRequest($request);

        if ($user->getId() != $bookmark->getUserId())
        {
            return $this->redirectToRoute('dashboard_home');
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bookmark);
            $em->flush();

            return $this->redirectToRoute('bookmark_index');
        }
        else
        {
            $errors = $editForm->getErrors();

            return $this->render('bookmark/edit.html.twig', array(
                'bookmark' => $bookmark,
                'edit_form' => $editForm->createView(),
                'user' => $user,
                'errors' => $errors,
            ));
        }

        return $this->render('bookmark/edit.html.twig', array(
            'bookmark' => $bookmark,
            'edit_form' => $editForm->createView(),
            'user' => $user,
            'errors' => $errors,
        ));
    }

    /**
     * Deletes a Bookmark entity.
     *
     * @Route("/delete/{id}", name="bookmark_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $bookmark = $em->getRepository('M4UDashboardBundle:Bookmark')->find($id);

        $em->remove($bookmark);
        $em->flush();
        return $this->redirectToRoute('bookmark_index');
    }

    /**
     * @return array
     * @View()
     */
    public function getBookmarksAction()
    {
        $bookmarks = $this->getDoctrine()->getRepository('M4UDashboardBundle:Bookmark')
            ->findAll();

        return ['bookmarks' => $bookmarks];
    }

    /**
     * Presents the form to use to create a new bookmark.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\View()
     *
     * @return FormTypeInterface
     */
    public function newBookmarkAction()
    {
        return $this->createForm(new BookmarkType());
    }

    /**
     * Fetch a Bookmark or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return BookmarkInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($bookmark = $this->container->get('m4u_dashboard.bookmark.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $bookmark;
    }

    /**
     * Creates a form to delete a Bookmark entity.
     *
     * @param Bookmark $bookmark The Bookmark entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bookmark $bookmark)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bookmark_delete', array('id' => $bookmark->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
