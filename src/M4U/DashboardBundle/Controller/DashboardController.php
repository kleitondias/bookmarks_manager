<?php

namespace M4U\DashboardBundle\Controller;

use AppBundle\Controller\BaseController;
use M4U\DashboardBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DashboardController extends BaseController
{
    /**
     * @Route("/", name="_dashboard")
     * @Template()
     */
    public function indexAction()
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_ADMIN'))
        {
            $users = $em->getRepository('M4UDashboardBundle:User')->findAll();
            $bookmarks = null;
        }
        else
        {
            $bookmarks = $em->getRepository('M4UDashboardBundle:Bookmark')->findBy(['userId' => $user->getId()]);
            $users = null;
        }

        return $this->render('bookmark/index.html.twig', array(
            'user' => $user,
            'bookmarks' => $bookmarks,
            'users' => $users,
        ));
    }

    /**
     * @Route("/users", name="users")
     * @Template()
     */
    public function usersAction()
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('M4UDashboardBundle:User')->findAll();

        return $this->render('dashboard/users.html.twig', array(
            'user' => $user,
            'users' => $users,
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/account/{id}", name="my_account")
     * @Template
     */
    public function accountAction(Request $request, User $user_entity)
    {
        $errors = null;
        $user = $this->getUser();

        if ($user->getId() != $user_entity->getId())
        {
            return $this->redirectToRoute('dashboard_home');
        }

        $editForm = $this->createForm('AppBundle\Form\Type\RegistrationType', $user_entity);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid())
        {
            $user = $editForm->getData();

            $user->setUpdatedAt(new \DateTime());

            $pwd = $user->getPassword();
            $encoder = $this->container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($user, $pwd);
            $user->setPassword($pwd);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('dashboard_home');
        }
        else
        {
            $errors = $editForm->getErrors();

            return $this->render('dashboard/account.html.twig', array(
                'user_entity' => $user_entity,
                'edit_form' => $editForm->createView(),
                'user' => $user,
                'errors' => $errors,
            ));
        }

        return $this->render('dashboard/account.html.twig', array(
            'user_entity' => $user_entity,
            'edit_form' => $editForm->createView(),
            'user' => $user,
            'errors' => $errors,
        ));
    }
}
