<?php

namespace M4U\DashboardBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use M4U\DashboardBundle\Model\BookmarkInterface;

class BookmarkHandler implements BookmarkHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
    }

    /**
     * Get a Bookmark.
     *
     * @param mixed $id
     *
     * @return BookmarkInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    private function createBookmark()
    {
        return new $this->entityClass();
    }

}