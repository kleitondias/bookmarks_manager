<?php

namespace M4U\DashboardBundle\Handler;

use M4U\DashboardBundle\Model\BookmarkInterface;

interface BookmarkHandlerInterface
{
    /**
     * Get a Bookmark given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return BookmarkInterface
     */
    public function get($id);
}