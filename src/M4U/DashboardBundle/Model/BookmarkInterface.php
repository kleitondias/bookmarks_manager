<?php

namespace M4U\DashboardBundle\Model;

Interface BookmarkInterface
{
    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return BookmarkInterface
     */
    public function setUserId($userId);

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BookmarkInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set url
     *
     * @param string $url
     *
     * @return BookmarkInterface
     */
    public function setUrl($url);

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BookmarkInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return BookmarkInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Set user
     *
     * @param \M4U\DashboardBundle\Entity\User $user
     *
     * @return BookmarkInterface
     */
    public function setUser(\M4U\DashboardBundle\Entity\User $user = null);

    /**
     * Get user
     *
     * @return \M4U\DashboardBundle\Entity\User
     */
    public function getUser();
}
