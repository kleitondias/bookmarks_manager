<?php

namespace M4U\DashboardBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="M4U\DashboardBundle\Repository\UserRepository")
 */
class User extends BaseUser
{

    const ADMIN = 'ROLE_ADMIN';
    const USER = 'ROLE_USER';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="logged_at", type="datetime", nullable=true)
     */
    protected $loggedAt;


    public function getId()
    {
        return $this->id;
    }

    public function eraseCredentials()
    {
        return;
    }

    public function serialize()
    {

    }

    public function unserialize($serialized)
    {

    }

    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set loggedAt
     *
     * @param \DateTime $loggedAt
     *
     * @return User
     */
    public function setLoggedAt($loggedAt)
    {
        $this->loggedAt = $loggedAt;

        return $this;
    }

    /**
     * Get loggedAt
     *
     * @return \DateTime
     */
    public function getLoggedAt()
    {
        return $this->loggedAt;
    }
    /**
     * @ORM\OneToMany(targetEntity="Bookmark", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     */
    protected $bookmarks;

    public function __construct()
    {
        parent::__construct();
        $this->bookmarks = new ArrayCollection();
    }

    /**
     * Add bookmark
     *
     * @param \M4U\DashboardBundle\Entity\Bookmark $bookmark
     *
     * @return User
     */
    public function addBookmark(\M4U\DashboardBundle\Entity\Bookmark $bookmark)
    {
        if (!$this->bookmarks->contains($bookmark)) {
            $this->bookmarks->add($bookmark);
        }

        return $this;
    }

    /**
     * Remove bookmark
     *
     * @param \M4U\DashboardBundle\Entity\Bookmark $bookmark
     */
    public function removeBookmark(\M4U\DashboardBundle\Entity\Bookmark $bookmark)
    {
        if ($this->bookmarks->contains($bookmark)) {
            $this->bookmarks->removeElement($bookmark);
        }

        return $this;
    }

    /**
     * Returns the true ArrayCollection of Bookmarks.
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getBookmarks()
    {
        return $this->bookmarks;
    }
}
